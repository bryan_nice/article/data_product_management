# Delivery Culture

## Co-Leads

### Data Product Manager

Top two:
+ Facilitate or provide clarity of vision
+ Provide contextual guardrails to enable dynamic dialogue

+ Data Analysis, Data model archetype previously (3-4)
+ Metrics framework development
+ Visualization
+ Understand the proto-cycle to iteratively
    + maturity of literacy
    + refining the product

### Delivery Lead

![delivery lead](assets/delivery_culture/delivery_lead.png)

Mindful of Internal Team Culture:
+ Cultivating safe space
+ Disagree and commit
+ Celebrate mistakes for opportunities to learn
+ Protect team boundaries
+ Facilitate team self-organization and commits
+ Grow humans to share skills and knowledge
+ Actively balance team commitments with completion

## New Team Formation

### 2 Weeks

### 30 Days

### 60 Days

### 90 Days

## Delivery Team

![delivery team](./assets/delivery_culture/delivery_team.png)

### CALMS

+ Culture: people come first
+ Automation: rely on tools for efficiency and repeatability
+ Lean: apply lean engineering practices to continuously improve
+ Measurement: use data to drive decisions and improvements
+ Sharing: share ideas, information, and goals across the delivery portfolio

Automation is not DevSecOps... Yet you are not able to do DevSecOps without
automation

### Culture

### Automation

### Lean

### Measurement

### Sharing

## Automation Factory

![automation_factory](./assets/delivery_culture/automation_factory.png)

## References

+ [CALMS for DevSecOps: Part 1 — Why Culture Is Critical](https://medium.com/identity-in-the-age-of-devops/calms-for-devsecops-part-1-why-culture-is-critical-ca2c45dfa645)
+ [CALMS for DevSecOps: Part 2 — Why You Need Automation](https://medium.com/identity-in-the-age-of-devops/calms-for-devsecops-part-2-why-you-need-automation-681d6c3e541f)
+ [CALMS for DevSecOps: Part 3 — How Lean Improves Performance](https://medium.com/identity-in-the-age-of-devops/calms-for-devsecops-part-3-how-lean-improves-performance-136896952a76)
+ [CALMS for DevSecOps: Part 4 — Measuring to Save Time and Avoid Breaches](https://medium.com/identity-in-the-age-of-devops/calms-for-devsecops-part-4-measuring-to-save-time-and-avoid-breaches-a231ec5f7f0e)
+ [CALMS for DevSecOps: Part 5 — The Power of Dojos and ChatOps in Security Knowledge Share](https://medium.com/identity-in-the-age-of-devops/calms-for-devsecops-part-5-the-power-of-dojos-and-chatops-in-security-knowledge-share-fb272971cb74)
