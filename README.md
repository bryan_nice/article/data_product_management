# Data Product Management

Data product management always has some degree of uncertainty associated to
the delivery lifecycle. It requires a few practices to curate a team culture
with psychological safety to experiment and learn per delivery iteration. Each
delivery iteration is designed to provide a build, measure, learn feedback loop
to enable the team to refine or pivot on the direction.

+ [Architectural Runway](https://www.scaledagileframework.com/architectural-runway/)
+ [Semantic Version 2.0.0](https://semver.org/)
