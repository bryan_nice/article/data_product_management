# Hypothesis Driven Development

## References

+ [HYPOTHESIS-DRIVEN DEVELOPMENT (PRACTITIONER’S GUIDE)](https://www.alexandercowan.com/hypothesis-driven-development-practitioners-guide/)
+ [The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses](https://www.amazon.com/Lean-Startup-Entrepreneurs-Continuous-Innovation-ebook/dp/B004J4XGN6/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=1636150777&sr=8-1)
