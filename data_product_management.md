# Data Product

## Overview

Data product design is a human usage centric approach to build and deliver
semantic value. This design approach is to provide a consistent method for
facilitating shared understanding of a given process, and the expected value
proposition outcome. This document is focused on providing the rationale behind
this design approach, context and definitions of what a data product is and is
not, and a repeatable process to curate shared understanding.

## Why?

Provides shared understanding of semantics for a given process, while growing
trust with our partners. All data products have some amount of uncertainty and
require integrated partnerships to quickly work through it. The goal per
iteration is to reduce the risk of uncertainty and time to deliver data products
aligned to clinical outcomes. As a result the methodology leverages the build,
measure, learn feedback loop to evolve our shared pool of understanding as the
data product is refined.

## What?

Data products are semantic relationships contextually aligned to value driven
outcomes for managing, and/or improving an organizational process. There are
three conceptual components to a data product; 1) interface layer, 2) semantic
layer, 3) physical layer. A human interface can be a visualization to consume
data, notification, alert or an input form. The semantic definition contains
descriptive context related and an organized view on the interactions associated
with a process. Physical data and related systems to capture, manage, and
retrieve it.

The main focus for designing data products is around the semantic definition
component. This is where applying usage centric domain design is most effective
for a specific clinical process, population, or subject area. It improves
discoverability and reusability of data products regardless of human interface
connected to it. Data products are not visualizations nor applications; however,
visualizations and applications are required for humans to interact with data
products.

## How?

There are four general steps to identify organizational semantics and design a
data product; 1) identify the process, 2) identify the steps within that process
, 3) identify the descriptions, and 4) identify the measures associated with
managing that process. Use these four steps to facilitate problem exploration
meetings. Use stories as practical documentation for a delivery team to flush
out the details of the physical design through data profiling and domain
modeling activities.

Within an iteration the delivery team can provide a few artifacts completed. 1)
data profiling documenting the population definition clearly describing the
inclusion criterion, exclusion criterion, completeness vs missingness, and
contextual assumptions. 2) conceptual model of a process with the measures and
descriptions incorporated. 3) an initial functional prototype of the data
product (optional, because this may be dependent on access or infrastructure
available). During the next partner meeting with clinicians, use these artifacts
to review with them to enable a shared understanding on where to refine or pivot
the next set of iterations.

## Product-Market Fit

![product_market_fit](./assets/data_product_management/data_product_market_fit.png)

## Product Maturity Phases

![product maturity phases](./assets/data_product_management/data_product_maturity_phases.png)

How Do We Get There...
+ Clarity of vision
+ Contextual guardrails
  + Enable self-organization
  + Enable dynamic dialogue

### Problem Exploration

### Problem Solution Fit

### Product Market Fit

### Scale It

### Protocycle

## What is an OKR?

![what is an okr](./assets/data_product_management/what_is_an_okr.png)

Objectives (Vision)
+ Explicitly address a target outcome
+ Align with long term mission, KPIs, product vision, and business goals
+ Evaluated for efficacy during a planning cycle
+ One per delivery team during a planning cycle

Key Results (Guardrails)
+ No more than five key results per objective
+ Time-bound to some cycle in which they are active
+ Explicitly define a target to be achieved
+ Provide scale of goodness to measure success
+ Focus on measurement of expected value
+ Use to monitor when objective is achieved

## Product Portfolio View

+ Where are we going?
+ How do we know we are aligned to the right outcome?

## References

+ [Desiging Data Products](https://towardsdatascience.com/designing-data-products-b6b93edf3d23)
+ [How to Build Great Data Products](https://hbr.org/2018/10/how-to-build-great-data-products)
+ [What is a data product?](https://terminusdb.com/blog/what-is-a-data-product/)
+ [Data Product Manager](https://www.productplan.com/glossary/data-product-manager/)
+ [Take a "Data as a Product" Approach to Democratize Data Access](https://www.k2view.com/blog/data-as-a-product/)
+ [Data as a product vs data products. What are the differences?](https://towardsdatascience.com/data-as-a-product-vs-data-products-what-are-the-differences-b43ddbb0f123)
+ [Nine Principles for Designing Great Data Products](https://www.frogdesign.com/designmind/nine-principles-for-designing-great-data-products)
+ [The Data Product Design Thinking Process](https://towardsdatascience.com/the-data-product-design-thinking-process-6b3eba561b2b)
+ [Alignment through OKR’s and Hypotheses](https://productcoalition.com/alignment-through-okrs-and-hypotheses-4f2b9bf94499)
+ [Product Planning with OKRs (Objectives & Key Results)](https://medium.com/blackstar/product-planning-with-okrs-objectives-key-results-660708675e96)
+ [Measure What Matters: How Google, Bono, and the Gates Foundation Rock the World with OKRs](https://www.amazon.com/Measure-What-Matters-Google-Foundation-ebook/dp/B078FZ9SYB/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=1636150696&sr=8-5)
